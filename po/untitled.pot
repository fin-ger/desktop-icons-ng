# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-09 13:49+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../askConfirmPopup.js:35 ../askNamePopup.js:33 ../desktopIconsUtil.js:170
msgid "Cancel"
msgstr ""

#: ../askConfirmPopup.js:36
msgid "Delete"
msgstr ""

#: ../askNamePopup.js:32
msgid "OK"
msgstr ""

#: ../askRenamePopup.js:37
msgid "Folder name"
msgstr ""

#: ../askRenamePopup.js:37
msgid "File name"
msgstr ""

#: ../askRenamePopup.js:44
msgid "Rename"
msgstr ""

#: ../desktopIconsUtil.js:74
msgid "Command not found"
msgstr ""

#: ../desktopIconsUtil.js:161
msgid "Do you want to run “{0}”, or display its contents?"
msgstr ""

#: ../desktopIconsUtil.js:162
msgid "“{0}” is an executable text file."
msgstr ""

#: ../desktopIconsUtil.js:166
msgid "Execute in a terminal"
msgstr ""

#: ../desktopIconsUtil.js:168
msgid "Show"
msgstr ""

#: ../desktopIconsUtil.js:172
msgid "Execute"
msgstr ""

#: ../desktopManager.js:196
msgid "Nautilus File Manager not found"
msgstr ""

#: ../desktopManager.js:197
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""

#: ../desktopManager.js:437
msgid "New Folder"
msgstr ""

#: ../desktopManager.js:443
msgid "Paste"
msgstr ""

#: ../desktopManager.js:447
msgid "Undo"
msgstr ""

#: ../desktopManager.js:451
msgid "Redo"
msgstr ""

#: ../desktopManager.js:457
msgid "Show Desktop in Files"
msgstr ""

#: ../desktopManager.js:461 ../fileItem.js:748
msgid "Open in Terminal"
msgstr ""

#: ../desktopManager.js:467
msgid "Change Background…"
msgstr ""

#: ../desktopManager.js:476
msgid "Display Settings"
msgstr ""

#: ../desktopManager.js:483 ../preferences.js:83
msgid "Settings"
msgstr ""

#: ../desktopManager.js:899 ../desktopManager.js:920
msgid "Error while deleting files"
msgstr ""

#: ../desktopManager.js:900
msgid "There was an error while trying to permanently delete the folder {:}."
msgstr ""

#: ../desktopManager.js:921
msgid "There was an error while trying to permanently delete the file {:}."
msgstr ""

#: ../desktopManager.js:944
msgid "Are you sure you want to permanently delete these items?"
msgstr ""

#: ../desktopManager.js:944
msgid "If you delete an item, it will be permanently lost."
msgstr ""

#: ../desktopManager.js:1054
msgid "New folder"
msgstr ""

#: ../fileItem.js:623
msgid "Don’t Allow Launching"
msgstr ""

#: ../fileItem.js:625
msgid "Allow Launching"
msgstr ""

#: ../fileItem.js:691
msgid "Open"
msgstr ""

#: ../fileItem.js:697
msgid "Open With Other Application"
msgstr ""

#: ../fileItem.js:704
msgid "Cut"
msgstr ""

#: ../fileItem.js:707
msgid "Copy"
msgstr ""

#: ../fileItem.js:711
msgid "Rename…"
msgstr ""

#: ../fileItem.js:715
msgid "Move to Trash"
msgstr ""

#: ../fileItem.js:719
msgid "Delete permanently"
msgstr ""

#: ../fileItem.js:732
msgid "Empty Trash"
msgstr ""

#: ../fileItem.js:740
msgid "Properties"
msgstr ""

#: ../fileItem.js:744
msgid "Show in Files"
msgstr ""

#: ../preferences.js:89
msgid "Size for the desktop icons"
msgstr ""

#: ../preferences.js:89
msgid "Small"
msgstr ""

#: ../preferences.js:89
msgid "Standard"
msgstr ""

#: ../preferences.js:89
msgid "Large"
msgstr ""

#: ../preferences.js:90
msgid "Show the personal folder in the desktop"
msgstr ""

#: ../preferences.js:91
msgid "Show the trash icon in the desktop"
msgstr ""

#: ../preferences.js:95
msgid "Settings shared with Nautilus"
msgstr ""

#: ../preferences.js:101
msgid "Click type for open files"
msgstr ""

#: ../preferences.js:101
msgid "Single click"
msgstr ""

#: ../preferences.js:101
msgid "Double click"
msgstr ""

#: ../preferences.js:102
msgid "Show hidden files"
msgstr ""

#: ../preferences.js:103
msgid "Show a context menu item to delete permanently"
msgstr ""

#: ../preferences.js:106
msgid "Action to do when launching a program from the desktop"
msgstr ""

#: ../preferences.js:107
msgid "Display the content of the file"
msgstr ""

#: ../preferences.js:108
msgid "Launch the file"
msgstr ""

#: ../preferences.js:109
msgid "Ask what to do"
msgstr ""

#: ../preferences.js:113
msgid "Show image thumbnails"
msgstr ""

#: ../preferences.js:114
msgid "Never"
msgstr ""

#: ../preferences.js:115
msgid "Local files only"
msgstr ""

#: ../preferences.js:116
msgid "Always"
msgstr ""

#: ../prefs.js:39
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Settings'"
msgstr ""

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:1
msgid "Icon size"
msgstr ""

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:2
msgid "Set the size for the desktop icons."
msgstr ""

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:3
msgid "Show personal folder"
msgstr ""

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:4
msgid "Show the personal folder in the desktop."
msgstr ""

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:5
msgid "Show trash icon"
msgstr ""

#: ../schemas/org.gnome.shell.extensions.ding.gschema.xml.h:6
msgid "Show the trash icon in the desktop."
msgstr ""
